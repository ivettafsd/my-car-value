# My car value
### was developed as a reliable template to building enterprise-ready apps with NestJS. It includes everything you need for authentication, authorization, automated testing, and production deployment.

## Features

- **Authentication/Authorization**: Fully covered to ensure secure access to your API endpoints, with support for cookie-based authentication.
- **Automated Testing**: Includes a comprehensive suite of tests to maintain code quality.
- **Production Deployment**: Setup for seamless deployment to production environments.
- **Data Persistence**: Includes data modeling and persistence configurations using TypeORM.
- **TypeScript Support**: Utilizes TypeScript for clean and error-free coding practices.
- **Customizable**: Easily tweak and extend according to any projects.

## Tech Skills and Libraries

- **NestJS**: A backend framework for building scalable and reliable APIs.
- **TypeORM**: Object-Relational Mapping (ORM) library for database interactions.
- **Jest**: JavaScript testing framework for unit and integration tests.
- **TypeScript**: Superset of JavaScript providing static typing and other features.
- **Express**: Fast, unopinionated, minimalist web framework for Node.js.


## Quick Start

```bash
$ git clone <repository_url>
```

## Install
```bash
$ npm install
```

## Running the app

```bash
# development
$ npm run start

# watch mode
$ npm run start:dev

# production mode
$ npm run start:prod
```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```

